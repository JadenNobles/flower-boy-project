# **Flower Boy**

## *TrackList:*
1. Foreword
2. Where This Flower Blooms
3. Sometimes...
4. See You Again
5. Who Dat Boy
6. Pothole
7. Garden Shed
8. Boredom
9. I Ain't Got Time!
10. 911 / Mr. Lonely
11. Droppin' Seeds
12. November
13. Glitter
14. Enjoy Right Now, Today

## *Longest Songs*
- Boredom
- 911 / Mr. Lonely
- Pothole
- Enjoy Right Now, Today

---

> "boredom got a new best friend" - Tyler the Creator

![Flower Boy](/flowerboy.jpg)

| Feature     | Song |
| ----------- | ----------- |
|  Rex Orange County      | Foreword       |
| Frank Ocean   | Where This Flower Blooms        |
| Jaden Smith   |  Pothole      |
|  ASAP Rocky   | Who Dat Boy   |
|Kali Uchis | See You Again|
| Estelle | Garden Shed |
| Lil Wayne | Droppin' Seeds|
